# Gunakan versi Node.js yang lebih baru
FROM node:18-alpine

# Setel direktori kerja
WORKDIR /usr/src/app

# Salin package.json dan package-lock.json ke direktori kerja
COPY package*.json ./

# Instal dependensi aplikasi
RUN npm install

# Salin file proyek ke dalam gambar Docker
COPY . .

# Paparkan port 39309 ke dunia luar
EXPOSE 3000

# Perintah untuk menjalankan aplikasi
CMD ["npm", "run", "dev"] 
